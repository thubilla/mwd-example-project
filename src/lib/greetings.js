//exports.world = function(){
//	console.log('Hello, World!');
//};

//exports.master = function(){
//	console.log('Hello, Master!');
//};

//module.exports = function(name = 'World'){
//	console.log(`Hello, ${name}!`);
//};

module.exports = function greet(name = 'World'){
	return `Hello, ${name}!`;
};
